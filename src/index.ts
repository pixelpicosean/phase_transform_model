import image_src from "./image.png";
const image = new Image;
image.src = image_src;

let canvas: HTMLCanvasElement = null;
let ctx: CanvasRenderingContext2D = null;

const inputs = [
    {
        position_x: null as HTMLInputElement,
        position_y: null as HTMLInputElement,
        rotation: null as HTMLInputElement,
        origin_x: null as HTMLInputElement,
        origin_y: null as HTMLInputElement,
        contentOffset_x: null as HTMLInputElement,
        contentOffset_y: null as HTMLInputElement,
        scale_x: null as HTMLInputElement,
        scale_y: null as HTMLInputElement,
    },
    {
        position_x: null as HTMLInputElement,
        position_y: null as HTMLInputElement,
        rotation: null as HTMLInputElement,
        origin_x: null as HTMLInputElement,
        origin_y: null as HTMLInputElement,
        contentOffset_x: null as HTMLInputElement,
        contentOffset_y: null as HTMLInputElement,
        scale_x: null as HTMLInputElement,
        scale_y: null as HTMLInputElement,
    },
    {
        position_x: null as HTMLInputElement,
        position_y: null as HTMLInputElement,
        rotation: null as HTMLInputElement,
        origin_x: null as HTMLInputElement,
        origin_y: null as HTMLInputElement,
        contentOffset_x: null as HTMLInputElement,
        contentOffset_y: null as HTMLInputElement,
        scale_x: null as HTMLInputElement,
        scale_y: null as HTMLInputElement,
    },
]

function clamp(value: number, min: number, max: number) {
    return (value < min) ? min : (value > max) ? max : value;
}

class Vector2 {
    constructor(
        public x = 0,
        public y = 0
    ) { }

    set(x: number, y: number) {
        this.x = x;
        this.y = y;
        return this;
    }

    clone() {
        return new Vector2(this.x, this.y);
    }

    add(other: Vector2) {
        this.x += other.x;
        this.y += other.y;
        return this;
    }

    multiply(other: Vector2) {
        this.x *= other.x;
        this.y *= other.y;
        return this;
    }

    floor() {
        this.x = Math.floor(this.x);
        this.y = Math.floor(this.y);
        return this;
    }
}

class Matrix {
    a = 1;
    b = 0;
    c = 0;
    d = 1;
    tx = 0;
    ty = 0;

    identity() {
        this.a = 1;
        this.b = 0;
        this.c = 0;
        this.d = 1;
        this.tx = 0;
        this.ty = 0;
        return this;
    }

    append(matrix: Matrix) {
        const a1 = this.a
        const b1 = this.b
        const c1 = this.c
        const d1 = this.d

        this.a = (matrix.a * a1) + (matrix.b * c1)
        this.b = (matrix.a * b1) + (matrix.b * d1)
        this.c = (matrix.c * a1) + (matrix.d * c1)
        this.d = (matrix.c * b1) + (matrix.d * d1)

        this.tx = (matrix.tx * a1) + (matrix.ty * c1) + this.tx
        this.ty = (matrix.tx * b1) + (matrix.ty * d1) + this.ty

        return this
    }

    rotate(angle: number) {
        const cos = Math.cos(angle)
        const sin = Math.sin(angle)

        const a1 = this.a
        const c1 = this.c
        const tx1 = this.tx

        this.a = (a1 * cos) - (this.b * sin)
        this.b = (a1 * sin) + (this.b * cos)
        this.c = (c1 * cos) - (this.d * sin)
        this.d = (c1 * sin) + (this.d * cos)
        this.tx = (tx1 * cos) - (this.ty * sin)
        this.ty = (tx1 * sin) + (this.ty * cos)

        return this
    }

    scale(x: number, y: number) {
        this.a *= x;
        this.b *= y;
        this.c *= x;
        this.d *= y;

        return this;
    }

    xform(point: Vector2, r_out = new Vector2) {
        const x = (this.a * point.x) + (this.c * point.y) + this.tx
        const y = (this.b * point.x) + (this.d * point.y) + this.ty
        return r_out.set(x, y)
    }
}

class Transform {
    size = new Vector2;
    origin = new Vector2;
    contentOffset = new Vector2;

    // user properties
    position = new Vector2;
    rotation = 0;
    scale = new Vector2(1, 1);
    skew = 0;

    getOriginPosition() {
        const originOffset = this.origin.clone()
            .multiply(this.size)
        ;
        return this.position.clone().add(originOffset);
    }
}

class Element {
    fill: HTMLImageElement = null;

    transform = new Transform;

    // action mode properties
    position = new Vector2;
    rotation = 0;
    scale = new Vector2(1, 1);
    skew = 0;

    getUserPosition() {
        return this.transform.position.clone()
            .add(this.position)
        ;
    }
    getUserSize() {
        return this.transform.size.clone();
    }

    getOriginWorldPosition() {
        return this.transform.getOriginPosition().add(this.position);
    }
}

enum InterpolateType {
    DISCRATE,
    LINEAR,
}

enum TrackType {
    NUMBER,
    VECTOR2,
}

interface Track {
    key: string;
    type: TrackType;
    times: number[];
    values: (Vector2|number)[];
    interpolateType: InterpolateType;
}

class Animation {
    constructor(
        public tracks: Track[] = []
    ) { }
}

function setValueNumber(obj: any, key: string, value: number) {
    if (key.indexOf('.') < 0) {
        obj[key] = value;
        return true;
    }

    const fields = key.split('.');
    let target = obj;
    for (let i = 0; i < fields.length - 1; i++) {
        target = target[fields[i]];
        if (i === fields.length - 2) {
            target[fields[i + 1]] = value;
            return true;
        }
    }
    return false;
}

function setValueVector2(obj: any, key: string, value: Vector2) {
    if (key.indexOf('.') < 0) {
        obj[key].x = value.x;
        obj[key].y = value.y;
        return true;
    }

    const fields = key.split('.');
    let target = obj;
    for (const key of fields) {
        target = target[key];
    }
    if (target) {
        target.x = value.x;
        target.y = value.y;
        return true;
    }
    return false;
}

function interpolateNumber(a: number, b: number, t: number) {
    t = clamp(t, 0, 1);
    return (b - a) * t + a;
}

const vec = new Vector2;
function interpolateVector2(a: Vector2, b: Vector2, t: number) {
    vec.x = interpolateNumber(a.x, b.x, t);
    vec.y = interpolateNumber(a.y, b.y, t);
    return vec;
}

class Animator {
    animation: Animation = null;
    target: any = null;

    seek(time: number) {
        for (const t of this.animation.tracks) {
            time_iter: for (let k = 0; k < t.times.length; k++) {
                const currKeyTime = t.times[k];
                const nextKeyTime = ((k + 1) < t.times.length) ? t.times[k + 1] : -1;

                // already the last key, use last value
                if (nextKeyTime < 0) {
                    switch (t.type) {
                        case TrackType.NUMBER: {
                            setValueNumber(this.target, t.key, t.values[k] as number);
                        } break;
                        case TrackType.VECTOR2: {
                            setValueVector2(this.target, t.key, t.values[k] as Vector2);
                        } break;
                    }
                    break time_iter;
                }

                // found 2 keys
                if (currKeyTime <= time && time <= nextKeyTime) {
                    const currValue = t.values[k];

                    switch (t.interpolateType) {
                        case InterpolateType.DISCRATE: {
                            switch (t.type) {
                                case TrackType.NUMBER: {
                                    setValueNumber(this.target, t.key, currValue as number);
                                } break;
                                case TrackType.VECTOR2: {
                                    setValueVector2(this.target, t.key, currValue as Vector2);
                                } break;
                            }

                            break time_iter;
                        } break;

                        case InterpolateType.LINEAR: {
                            const nextValue = t.values[k + 1];
                            switch (t.type) {
                                case TrackType.NUMBER: {
                                    const value = interpolateNumber(currValue as number, nextValue as number, (time - currKeyTime) / (nextKeyTime - currKeyTime));
                                    setValueNumber(this.target, t.key, value);
                                } break;
                                case TrackType.VECTOR2: {
                                    const value = interpolateVector2(currValue as Vector2, nextValue as Vector2, (time - currKeyTime) / (nextKeyTime - currKeyTime));
                                    setValueVector2(this.target, t.key, value);
                                } break;
                            }

                            break time_iter;
                        } break;
                    }
                }
            }
        }
    }
}

const anim_IdleRotate = new Animation([
    {
        key: "transform.position",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                100              ],
        values: [new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0)],
        interpolateType: InterpolateType.LINEAR,
    },
    {
        key: "transform.rotation",
        type: TrackType.NUMBER,
        times:  [0, 50,            100    ],
        values: [0, Math.PI * 0.5, Math.PI],
        interpolateType: InterpolateType.LINEAR,
    },
    {
        key: "transform.scale",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                100              ],
        values: [new Vector2(1, 1), new Vector2(1, 1), new Vector2(1, 1)],
        interpolateType: InterpolateType.LINEAR,
    },
    {
        key: "transform.origin",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                100              ],
        values: [new Vector2(1, 1), new Vector2(2, 1), new Vector2(2, 1)],
        interpolateType: InterpolateType.DISCRATE,
    },
    {
        key: "transform.contentOffset",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                  100                ],
        values: [new Vector2(0, 0), new Vector2(-1, -1), new Vector2(-1, -1)],
        interpolateType: InterpolateType.DISCRATE,
    },
]);

const anim_IdleRotateInterp = new Animation([
    {
        key: "transform.position",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                100              ],
        values: [new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0)],
        interpolateType: InterpolateType.LINEAR,
    },
    {
        key: "transform.rotation",
        type: TrackType.NUMBER,
        times:  [0, 50,            100    ],
        values: [0, Math.PI * 0.5, Math.PI],
        interpolateType: InterpolateType.LINEAR,
    },
    {
        key: "transform.scale",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                100              ],
        values: [new Vector2(1, 1), new Vector2(1, 1), new Vector2(1, 1)],
        interpolateType: InterpolateType.LINEAR,
    },
    {
        key: "transform.origin",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                100              ],
        values: [new Vector2(1, 1), new Vector2(2, 1), new Vector2(2, 1)],
        interpolateType: InterpolateType.LINEAR,
    },
    {
        key: "transform.contentOffset",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                  100                ],
        values: [new Vector2(0, 0), new Vector2(-1, -1), new Vector2(-1, -1)],
        interpolateType: InterpolateType.LINEAR,
    },
]);

const anim_MoveRotate = new Animation([
    {
        key: "transform.position",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                  100                ],
        values: [new Vector2(0, 0), new Vector2(100, 0), new Vector2(200, 0)],
        interpolateType: InterpolateType.LINEAR,
    },
    {
        key: "transform.rotation",
        type: TrackType.NUMBER,
        times:  [0, 50,            100    ],
        values: [0, Math.PI * 0.5, Math.PI],
        interpolateType: InterpolateType.LINEAR,
    },
    {
        key: "transform.scale",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                100              ],
        values: [new Vector2(1, 1), new Vector2(1, 1), new Vector2(1, 1)],
        interpolateType: InterpolateType.LINEAR,
    },
    {
        key: "transform.origin",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                100              ],
        values: [new Vector2(1, 1), new Vector2(2, 1), new Vector2(2, 1)],
        interpolateType: InterpolateType.DISCRATE,
    },
    {
        key: "transform.contentOffset",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                  100                ],
        values: [new Vector2(0, 0), new Vector2(-1, -1), new Vector2(-1, -1)],
        interpolateType: InterpolateType.DISCRATE,
    },
]);

const anim_MoveScale = new Animation([
    {
        key: "transform.position",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                100              ],
        values: [new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0)],
        interpolateType: InterpolateType.LINEAR,
    },
    {
        key: "transform.rotation",
        type: TrackType.NUMBER,
        times:  [0, 50,            100    ],
        values: [0, Math.PI * 0.5, Math.PI],
        interpolateType: InterpolateType.LINEAR,
    },
    {
        key: "transform.scale",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                100              ],
        values: [new Vector2(1, 1), new Vector2(1, 2), new Vector2(1, 1)],
        interpolateType: InterpolateType.LINEAR,
    },
    {
        key: "transform.origin",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                100              ],
        values: [new Vector2(1, 1), new Vector2(3, 1), new Vector2(2, 1)],
        interpolateType: InterpolateType.DISCRATE,
    },
    {
        key: "transform.contentOffset",
        type: TrackType.VECTOR2,
        times:  [0,                 50,                  100                ],
        values: [new Vector2(0, 0), new Vector2(-2, -1), new Vector2(-1, -1)],
        interpolateType: InterpolateType.DISCRATE,
    },
]);

const BOX_SIZE = 50;

function getMousePos(r_out: Vector2, e: MouseEvent) {
    const box = canvas.getBoundingClientRect();
    const scaleX = canvas.width / box.width;
    const scaleY = canvas.height / box.height;
    r_out.x = (e.clientX - box.left) * scaleX;
    r_out.y = (e.clientY - box.top) * scaleY;
    return r_out;
}

function selectAnim(idx: number) {
    state.selectedAnimIdx = idx;
    const anim = state.anims[idx];

    for (let k = 0; k < inputs.length; k++) {
        const pos = anim.tracks[0].values[k] as Vector2;
        const rot = anim.tracks[1].values[k] as number;
        const scale = anim.tracks[2].values[k] as Vector2;
        const origin = anim.tracks[3].values[k] as Vector2;
        const contentOffset = anim.tracks[4].values[k] as Vector2;

        inputs[k].position_x.value = `${pos.x}`;
        inputs[k].position_y.value = `${pos.y}`;

        inputs[k].rotation.value = `${Math.round(rot / Math.PI * 180)}`;

        inputs[k].scale_x.value = `${scale.x}`;
        inputs[k].scale_y.value = `${scale.y}`;

        inputs[k].origin_x.value = `${origin.x}`;
        inputs[k].origin_y.value = `${origin.y}`;

        inputs[k].contentOffset_x.value = `${contentOffset.x}`;
        inputs[k].contentOffset_y.value = `${contentOffset.y}`;
    }
}

function clear(top = 0) {
    ctx.fillStyle = "white";
    ctx.fillRect(0, top, canvas.width, canvas.height - top);
}

function drawGrid() {
    const step = BOX_SIZE;

    ctx.lineWidth = 1;
    ctx.strokeStyle = "#333";
    ctx.beginPath();
    for (let r = 0; r < canvas.height; r += step) {
        ctx.moveTo(0, r);
        ctx.lineTo(canvas.width, r);
    }
    for (let q = 0; q < canvas.width; q += step) {
        ctx.moveTo(q, 0);
        ctx.lineTo(q, canvas.height);
    }
    ctx.stroke();
}

function drawUserPosition(x: number, y: number) {
    const RADIUS = BOX_SIZE * 0.1;

    ctx.beginPath();
    ctx.fillStyle = "#f00";
    ctx.arc(x, y, RADIUS, 0, Math.PI * 2);
    ctx.fill();
    ctx.strokeStyle = "#000";
    ctx.lineWidth = BOX_SIZE * 0.04;
    ctx.stroke();
}

function drawOrigin(x: number, y: number) {
    const RADIUS = BOX_SIZE * 0.1;

    ctx.beginPath();
    ctx.fillStyle = "#0f0";
    ctx.arc(x, y, RADIUS, 0, Math.PI * 2);
    ctx.fill();
    ctx.strokeStyle = "#000";
    ctx.lineWidth = BOX_SIZE * 0.04;
    ctx.stroke();
}

function drawElement(e: Element) {
    const userPosition = e.getUserPosition();
    const userSize = e.getUserSize();

    const originOffset = e.transform.origin.clone()
        .multiply(e.transform.size)
    ;
    const originLocal = e.transform.getOriginPosition();

    const contentOffset = e.transform.contentOffset.clone()
        .multiply(e.transform.size)
    ;

    const m = new Matrix;
    m.tx += e.position.x;
    m.ty += e.position.y;

    const m2 = new Matrix;
    m2.scale(e.transform.scale.x, e.transform.scale.y);
    m2.rotate(e.transform.rotation);
    m2.tx = originLocal.x;
    m2.ty = originLocal.y;

    const m3 = new Matrix;
    m3.tx = -originOffset.x * 2 - contentOffset.x;
    m3.ty = -originOffset.y * 2 - contentOffset.y;

    m.append(m2).append(m3);

    ctx.setTransform(m.a, m.b, m.c, m.d, m.tx, m.ty);
    ctx.drawImage(
        e.fill,
        originOffset.x, originOffset.y,
        userSize.x, userSize.y
    );
    ctx.resetTransform();

    // draw bounding box
    ctx.strokeStyle = "#ea020b";
    ctx.lineWidth = BOX_SIZE * 0.06;
    ctx.strokeRect(userPosition.x, userPosition.y, userSize.x, userSize.y);

    // draw user position
    drawUserPosition(userPosition.x, userPosition.y);

    // draw origin
    const originWorld = e.getOriginWorldPosition();
    drawOrigin(originWorld.x, originWorld.y);
}

function drawElementInfo(e: Element, left: number, top: number) {
    const space = 20;

    ctx.fillStyle = "#000";
    ctx.font = "14px Verdana";

    const userPosition = e.getUserPosition().floor();
    ctx.fillText(`user_position: (${userPosition.x}, ${userPosition.y})`, left, top); top += space;

    const scale = e.transform.scale.clone();
    ctx.fillText(`user_position: (${scale.x.toFixed(2)}, ${scale.y.toFixed(2)})`, left, top); top += space;

    const originWorld = e.getOriginWorldPosition().floor();
    ctx.fillText(`origin: (${Math.floor(e.transform.origin.x * 100)}%, ${Math.floor(e.transform.origin.y * 100)}%)`, left, top); top += space;
    ctx.fillText(`origin_position: (${originWorld.x}, ${originWorld.y})`, left, top); top += space;

    ctx.fillText(`contentOffset: (${Math.floor(e.transform.contentOffset.x * 100)}%, ${Math.floor(e.transform.contentOffset.y * 100)}%)`, left, top); top += space;

    ctx.fillText(`rotation: ${(e.transform.rotation / Math.PI).toFixed(2)} PI`, left, top); top += space;
}

const state = {
    idle: new Element,
    anim_idle: new Animator,

    idle_interp: new Element,
    anim_idle_interp: new Animator,

    move: new Element,
    anim_move: new Animator,

    scale: new Element,
    anim_scale: new Animator,

    anims: [
        anim_IdleRotate,
        anim_IdleRotateInterp,
        anim_MoveRotate,
        anim_MoveScale,
    ],

    selectedAnimIdx: 0,
};

let slide: HTMLInputElement = null;

function init() {
    canvas.width = BOX_SIZE * (3 * 6 + 2);
    canvas.height = (
        BOX_SIZE * 3 * 2    // static
        +
        BOX_SIZE * 3        // idle animation
        +
        BOX_SIZE * 3        // idle animation with origin interpolation
        +
        BOX_SIZE * 3        // move animation
        +
        BOX_SIZE * 3        // scale animation
    );

    // key inputs
    for (let k = 0; k < inputs.length; k++) {
        inputs[k] = {
            position_x: document.getElementById(`key${k + 1}_position_x`) as HTMLInputElement,
            position_y: document.getElementById(`key${k + 1}_position_y`) as HTMLInputElement,

            rotation: document.getElementById(`key${k + 1}_rotation`) as HTMLInputElement,

            origin_x: document.getElementById(`key${k + 1}_origin_x`) as HTMLInputElement,
            origin_y: document.getElementById(`key${k + 1}_origin_y`) as HTMLInputElement,

            contentOffset_x: document.getElementById(`key${k + 1}_content_offset_x`) as HTMLInputElement,
            contentOffset_y: document.getElementById(`key${k + 1}_content_offset_y`) as HTMLInputElement,

            scale_x: document.getElementById(`key${k + 1}_scale_x`) as HTMLInputElement,
            scale_y: document.getElementById(`key${k + 1}_scale_y`) as HTMLInputElement,
        };

        inputs[k].position_x.onchange = (e) => {
            const anim = state.anims[state.selectedAnimIdx];
            (anim.tracks[0].values[k] as Vector2).x = (e.target as HTMLInputElement).valueAsNumber;
        };
        inputs[k].position_y.onchange = (e) => {
            const anim = state.anims[state.selectedAnimIdx];
            (anim.tracks[0].values[k] as Vector2).y = (e.target as HTMLInputElement).valueAsNumber;
        };

        inputs[k].rotation.onchange = (e) => {
            const anim = state.anims[state.selectedAnimIdx];
            (anim.tracks[1].values[k] as number) = (e.target as HTMLInputElement).valueAsNumber;
        };

        inputs[k].scale_x.onchange = (e) => {
            const anim = state.anims[state.selectedAnimIdx];
            (anim.tracks[2].values[k] as Vector2).x = (e.target as HTMLInputElement).valueAsNumber;
        };
        inputs[k].scale_y.onchange = (e) => {
            const anim = state.anims[state.selectedAnimIdx];
            (anim.tracks[2].values[k] as Vector2).y = (e.target as HTMLInputElement).valueAsNumber;
        };

        inputs[k].origin_x.onchange = (e) => {
            const anim = state.anims[state.selectedAnimIdx];
            (anim.tracks[3].values[k] as Vector2).x = (e.target as HTMLInputElement).valueAsNumber;
        };
        inputs[k].origin_y.onchange = (e) => {
            const anim = state.anims[state.selectedAnimIdx];
            (anim.tracks[3].values[k] as Vector2).y = (e.target as HTMLInputElement).valueAsNumber;
        };

        inputs[k].contentOffset_x.onchange = (e) => {
            const anim = state.anims[state.selectedAnimIdx];
            (anim.tracks[4].values[k] as Vector2).x = (e.target as HTMLInputElement).valueAsNumber;
        };
        inputs[k].contentOffset_y.onchange = (e) => {
            const anim = state.anims[state.selectedAnimIdx];
            (anim.tracks[4].values[k] as Vector2).y = (e.target as HTMLInputElement).valueAsNumber;
        };
    }

    {
        canvas.onmousedown = (e) => {
            const pos = getMousePos(new Vector2, e);
            const row = Math.floor((pos.y - BOX_SIZE * 6) / (BOX_SIZE * 3));
            if (row >= 0 && row <= 3) {
                selectAnim(row);
            }
        };
    }

    clear();
    drawGrid();

    // static demo 1
    {
        // key 1
        for (let i = 0; i < 3; i++) {
            const e = new Element;
            e.fill = image;

            e.transform.size.set(BOX_SIZE, BOX_SIZE);
            e.transform.origin.set(1, 1);
            e.transform.contentOffset.set(0, 0);

            e.position.set(BOX_SIZE + BOX_SIZE * 3 * i, BOX_SIZE);
            e.transform.rotation = Math.PI * 0.25 * i;

            drawElement(e);
        }

        // key 2
        for (let i = 0; i < 3; i++) {
            const e = new Element;
            e.fill = image;

            e.transform.size.set(BOX_SIZE, BOX_SIZE);
            // immediate key `origin`
            e.transform.origin.set(2, 1);
            // immediate key `contentOffset`
            e.transform.contentOffset.set(-1, -1);

            e.position.set(BOX_SIZE + BOX_SIZE * 3 * (i + 3), BOX_SIZE);
            // interpolate key `rotation`
            e.transform.rotation = Math.PI * 0.5; // value from key 2
            e.transform.rotation += Math.PI * 0.25 * i;

            drawElement(e);
        }
    }

    // static demo 2
    {
        // key 1
        for (let i = 0; i < 3; i++) {
            const e = new Element;
            e.fill = image;

            e.transform.size.set(BOX_SIZE, BOX_SIZE);
            e.transform.origin.set(1, 1);
            e.transform.contentOffset.set(0, 0);

            e.position.set(BOX_SIZE + BOX_SIZE * 3 * i, BOX_SIZE * 4);
            e.transform.rotation = Math.PI * 0.25 * i;

            drawElement(e);
        }

        // key 2
        for (let i = 0; i < 3; i++) {
            const e = new Element;
            e.fill = image;

            e.transform.size.set(BOX_SIZE, BOX_SIZE);
            // immediate key `origin`
            e.transform.origin.set(1, 1);
            e.transform.contentOffset.set(0, -1);

            e.position.set(BOX_SIZE + BOX_SIZE * 3 * (i + 3) + BOX_SIZE, BOX_SIZE * 4);
            e.transform.rotation = Math.PI * 0.5; // value from key 2
            e.transform.rotation += Math.PI * 0.25 * i;

            drawElement(e);
        }
    }

    // dynamic demo
    {
        let top = BOX_SIZE * 7
        const step = BOX_SIZE * 3

        // idle rect
        {
            state.idle.fill = image;

            state.idle.transform.size.set(BOX_SIZE, BOX_SIZE);
            state.idle.transform.origin.set(1, 1);
            state.idle.transform.contentOffset.set(0, 0);

            state.idle.position.set(BOX_SIZE + BOX_SIZE * 3, top);

            state.anim_idle.animation = anim_IdleRotate;
            state.anim_idle.target = state.idle;
            state.anim_idle.seek(0);

            top += step;
        }

        // idle rect with origin interpolation
        {
            state.idle_interp.fill = image;

            state.idle_interp.transform.size.set(BOX_SIZE, BOX_SIZE);
            state.idle_interp.transform.origin.set(1, 1);
            state.idle_interp.transform.contentOffset.set(0, 0);

            state.idle_interp.position.set(BOX_SIZE + BOX_SIZE * 3, top);

            state.anim_idle_interp.animation = anim_IdleRotateInterp;
            state.anim_idle_interp.target = state.idle_interp;
            state.anim_idle_interp.seek(0);

            top += step;
        }

        // move rect
        {
            state.move.fill = image;

            state.move.transform.size.set(BOX_SIZE, BOX_SIZE);
            state.move.transform.origin.set(1, 1);
            state.move.transform.contentOffset.set(0, 0);

            state.move.position.set(BOX_SIZE + BOX_SIZE * 3, top);

            state.anim_move.animation = anim_MoveRotate;
            state.anim_move.target = state.move;
            state.anim_move.seek(0);

            top += step;
        }

        // scale rect
        {
            state.scale.fill = image;

            state.scale.transform.size.set(BOX_SIZE, BOX_SIZE);
            state.scale.transform.origin.set(0.5, 1);
            state.scale.transform.contentOffset.set(0, 0);

            state.scale.position.set(BOX_SIZE + BOX_SIZE * 3, top);

            state.anim_scale.animation = anim_MoveScale;
            state.anim_scale.target = state.scale;
            state.anim_scale.seek(0);

            top += step;
        }
    }

    selectAnim(0);
}

function mainloop() {
    clear(BOX_SIZE * 6);

    // dynamic demo
    {
        const left = BOX_SIZE * 12;
        let top = BOX_SIZE * 6.5;

        {
            ctx.fillStyle = "#000";
            ctx.font = "16px Verdana";
            ctx.fillText(`Animate rotation only`, 10, top + BOX_SIZE);

            state.anim_idle.seek(slide.valueAsNumber);
            drawElement(state.idle);
            drawElementInfo(state.idle, left, top); top += BOX_SIZE * 3;
        }

        {
            ctx.fillStyle = "#000";
            ctx.font = "16px Verdana";
            ctx.fillText(`Animate rotation with`, 10, top + BOX_SIZE - 5);
            ctx.fillText(`interpolated origin`, 10, top + BOX_SIZE + 24 - 5);

            state.anim_idle_interp.seek(slide.valueAsNumber);
            drawElement(state.idle_interp);
            drawElementInfo(state.idle_interp, left, top); top += BOX_SIZE * 3;
        }

        {
            ctx.fillStyle = "#000";
            ctx.font = "16px Verdana";
            ctx.fillText(`Animate rotation`, 10, top + BOX_SIZE - 5);
            ctx.fillText(`and user_position`, 10, top + BOX_SIZE + 24 - 5);

            state.anim_move.seek(slide.valueAsNumber);
            drawElement(state.move);
            drawElementInfo(state.move, left, top); top += BOX_SIZE * 3;
        }

        {
            ctx.fillStyle = "#000";
            ctx.font = "16px Verdana";
            ctx.fillText(`Animate position`, 10, top + BOX_SIZE - 5);
            ctx.fillText(`and scale`, 10, top + BOX_SIZE + 24 - 5);

            state.anim_scale.seek(slide.valueAsNumber);
            drawElement(state.scale);
            drawElementInfo(state.scale, left, top); top += BOX_SIZE * 3;
        }
    }

    // draw select box
    {
        const padding = 4;

        ctx.resetTransform();
        ctx.lineWidth = 4;
        ctx.strokeStyle = "#bc0";
        ctx.strokeRect(padding, BOX_SIZE * 6 + state.selectedAnimIdx * (BOX_SIZE * 3) + padding, canvas.width - padding * 2, BOX_SIZE * 3 - padding * 2);
    }

    requestAnimationFrame(mainloop);
}

window.onload = () => {
    canvas = document.getElementById("canvas") as HTMLCanvasElement;
    ctx = canvas.getContext("2d");

    slide = document.querySelector(".timeline");

    init();
    mainloop();
};
