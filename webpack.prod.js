const { DefinePlugin } = require("webpack");
const { merge } = require("webpack-merge");

const common = require("./webpack.common");

module.exports = merge(common, {
    mode: "production",

    plugins: [
        new DefinePlugin({
            __DEV__: JSON.stringify(false),
        })
    ],
})
