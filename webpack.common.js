const path = require("path");
const fs = require("fs");

const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const module_alias = fs.readdirSync(path.resolve(__dirname, "src"), { withFileTypes: true })
    .filter(item => item.isDirectory())
    .reduce((alias, dir) => {
        alias[dir.name] = path.resolve(__dirname, "src", dir.name);
        return alias;
    }, {})

module.exports = {
    entry: "./src/index.ts",
    output: {
        filename: "app.js",
        path: path.resolve(__dirname, "dist"),
        pathinfo: false,
    },

    resolve: {
        extensions: [".ts", ".js", ".json"],

        preferRelative: true,
        alias: module_alias,
    },
    module: {
        rules: [
            // source code
            {
                test: /\.ts$/,
                use: {
                    loader: "@sucrase/webpack-loader",
                    options: {
                        transforms: ["typescript"],
                    },
                },
            },
            // shaders
            {
                test: /\.(vert|frag|vs|fs)$/,
                include: [
                    path.resolve(__dirname, "src"),
                ],
                loader: "raw-loader",
            },
            // binary resources packed as base64
            {
                test: /\.(jpe?g|png|ttf|eot|svg|woff(2)?|mp3)(\?[a-z0-9=&.]+)?$/,
                include: [
                    path.resolve(__dirname),
                ],
                loader: "base64-inline-loader",
            },
        ],
    },

    plugins: [
        new CleanWebpackPlugin,
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "index.html"),
        }),
    ],
}
