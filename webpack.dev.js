const { DefinePlugin } = require("webpack");
const { merge } = require("webpack-merge");

const common = require("./webpack.common");

module.exports = merge(common, {
    mode: "development",

    devtool: "source-map",
    devServer: {
        port: 4000,
    },

    plugins: [
        new DefinePlugin({
            __DEV__: JSON.stringify(true),
        })
    ],
})
