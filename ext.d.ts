// shader
declare module "*.vert" {
    const value: string;
    export default value;
}
declare module "*.vs" {
    const value: string;
    export default value;
}
declare module "*.frag" {
    const value: string;
    export default value;
}
declare module "*.fs" {
    const value: string;
    export default value;
}

// image
declare module "*.png" {
    const value: string;
    export default value;
}
declare module "*.jpg" {
    const value: string;
    export default value;
}
declare module "*.jpeg" {
    const value: string;
    export default value;
}

// font
declare module "*.ttf" {
    const value: string;
    export default value;
}
declare module "*.eot" {
    const value: string;
    export default value;
}
declare module "*.svg" {
    const value: string;
    export default value;
}
declare module "*.woff" {
    const value: string;
    export default value;
}
declare module "*.woff2" {
    const value: string;
    export default value;
}

// sound
declare module "*.mp3" {
    const value: string;
    export default value;
}
